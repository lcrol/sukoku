from boards.Board import Board


class NaiveBackTrackSolver(Board):
    """
    Sudoku solving object that uses recursive backtracking to solve. While slower than fast solver
    it will be used for visual solving as this algorithm is easy to understand visually
    """

    def __init__(self, sudoku_string=""):
        super().__init__(sudoku_string)

    def solve(self):
        calc_board = NaiveBackTrackSolver(sudoku_string=self.state_string)
        self.__solve(calc_board)
        self.load_board(calc_board.state_string)
        self.save_state_string()

    def __solve(self, board):
        pos = [0, 0]
        if not self.find_empty(board, pos):
            return True
        row = pos[0]
        col = pos[1]
        for number in range(1, 10):
            if self.is_safe(board, row, col, number):
                board.set_value(row, col, number)
                if self.__solve(board):
                    board.save_state_string()
                    return True
                board.set_value(row, col, 0)
        return False
