class Board:

    def __init__(self, sudoku_string=""):
        self.size = 9
        # Initialize the board to 9x9 of zeros
        self._board = [[0] * 9 for i in range(self.size)]
        # Private and constant, used to reinitialize the board.
        # Apply .rstrip() to remove weird chars
        self._STRING_REPRESENTATION = sudoku_string.rstrip()
        # Can change as the board gets updated and player/solver updates boards
        self.state_string = sudoku_string
        self.load_board(self._STRING_REPRESENTATION)

    def load_board(self, sudoku_string):
        if len(sudoku_string) != 81:
            raise ValueError("Sudoku string but be 81 Characters")
        for i in range(self.size):
            for j in range(self.size):
                self._board[i][j] = int(sudoku_string[i * self.size + j])

    def __repr__(self):
        string = ''
        for i in range(len(self._board)):
            for j in range(len(self._board[i])):
                string += str(self._board[i][j])
        return string

    def __str__(self):
        string = ''
        row_num = 0
        for row in self._board:
            if row_num % 3 == 0 and row_num != 0:
                string += '\n'
            string += str(row[0:3]) + '\t' + str(row[3:6]) + '\t' + str(row[6:9]) + "\n"
            row_num += 1
        return string

    def is_valid(self):
        for rows in range(self.size):
            for cols in range(self.size):
                if self.get_value(rows, cols) == 0:
                    continue
                if not self.validate_box(rows - rows % 3, cols - cols % 3, self.get_value(rows, cols)) \
                        or not self.validate_cols(cols, self.get_value(rows, cols)) \
                        or not self.validate_rows(rows, self.get_value(rows, cols)):
                    return False
        return True

    def validate_box(self, row, col, value):
        num_count = 0
        for i in range(3):
            for j in range(3):
                if self._board[i + row][j + col] == value:
                    num_count += 1
        return num_count < 2

    def validate_cols(self, col, value):
        num_count = 0
        for i in range(self.size):
            if self._board[i][col] == value:
                num_count += 1
        return num_count < 2

    def validate_rows(self, row, value):
        num_count = 0
        for i in range(self.size):
            if self._board[row][i] == value:
                num_count += 1
        return num_count < 2

    def set_value(self, i, j, value):
        self._board[i][j] = value

    def reload(self):
        self.load_board(self._STRING_REPRESENTATION)

    def get_value(self, i, j):
        return self._board[i][j]

    def save_state_string(self):
        self.state_string = ""
        for i in range(self.size):
            for j in range(self.size):
                self.state_string += str(self.get_value(i, j))

    def has_zeros(self):
        for i in range(self.size):
            for j in range(self.size):
                if self._board[i][j] == 0:
                    return True
        return False

    def is_solved(self):
        if self.is_valid() and not self.has_zeros():
            return True
        return False

    @staticmethod
    def check_cols(board, col, number):
        for i in range(9):
            if board.get_value(i, col) == number:
                return True
        return False

    @staticmethod
    def check_rows(board, row, number):
        for i in range(9):
            if board.get_value(row, i) == number:
                return True
        return False

    @staticmethod
    def check_box(board, row, col, number):
        for i in range(3):
            for j in range(3):
                if board.get_value(i + row, j + col) == number:
                    return True
        return False

    def is_safe(self, board, row, col, number):
        return not self.check_box(board, row - row % 3, col - col % 3, number) \
               and not self.check_cols(board, col, number) and not self.check_rows(board, row, number)

    @staticmethod
    def find_empty(board, pos):
        for i in range(9):
            for j in range(9):
                if board.get_value(i, j) == 0:
                    pos[0] = i
                    pos[1] = j
                    return True
        return False
