from boards.Board import Board


class AlphaBeta(Board):
    """
    Sudoku solving object that uses fast-solving algo to solve. used for occasion that
    performance solutions are needed
    """
    def __init__(self, sudoku_string=""):
        super().__init__(sudoku_string)

    def solve(self):
        calc_board = AlphaBeta(sudoku_string=self.state_string)
        self.__solve(calc_board)
        self.load_board(calc_board.state_string)
        self.save_state_string()

    # TODO
    def __solve(self, calc_board: Board) -> bool:
        pass
