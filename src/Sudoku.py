import pygame
import random
from boards.NaiveBackTrackSolver import NaiveBackTrackSolver


class Sudoku:

    def __init__(self):
        # Globals
        self._WHITE = (217, 217, 217)
        self._BLACK = (51, 51, 51)
        self._RED = (153, 0, 0)
        self.FPS = 60
        self.game_exit = False
        self.buffer = 100
        self.size = 1000
        self.meter = (self.size - 2 * self.buffer) / 9

        # Pygame __init__()
        pygame.init()
        self.game_display = pygame.display.set_mode((self.size, self.size))
        pygame.display.set_caption("Pydoku")
        self.clock = pygame.time.Clock()
        self.font = pygame.font.Font('freesansbold.ttf', 32)

        # board __init__()
        with open("../data/puzzles.txt", "r") as file:
            number_puzzles = int(file.readline())
            puzzles = file.readlines()
            current_puzzle = puzzles[random.randint(0, number_puzzles)]
            self.board = NaiveBackTrackSolver(sudoku_string=current_puzzle)
            file.close()

    def draw_board_grid(self):
        """
        draws the 9x9 grid for sudoku. Helper function for the draw_board func.
        :return:
        """

        pygame.draw.line(self.game_display, self._BLACK,
                         (self.buffer, self.buffer), (self.buffer, self.size - self.buffer), 6)
        pygame.draw.line(self.game_display, self._BLACK,
                         (self.buffer, self.buffer), (self.size - self.buffer, self.buffer), 6)
        pygame.draw.line(self.game_display, self._BLACK,
                         (self.size - self.buffer, self.size - self.buffer), (self.size - self.buffer, self.buffer), 6)
        pygame.draw.line(self.game_display, self._BLACK,
                         (self.size - self.buffer, self.size - self.buffer), (self.buffer, self.size - self.buffer), 6)

        for i in range(9):
            if i % 3 == 0 and i != 0:
                pygame.draw.line(self.game_display, self._BLACK, (self.buffer, self.buffer + (i * self.meter)),
                                 (self.size - self.buffer, self.buffer + (i * self.meter)), 6)
                continue
            pygame.draw.line(self.game_display, self._BLACK, (self.buffer, self.buffer + (i * self.meter)),
                             (self.size - self.buffer, self.buffer + (i * self.meter)), 3)

        for k in range(9):
            if k % 3 == 0 and k != 0:
                pygame.draw.line(self.game_display, self._BLACK, (self.buffer + (k * self.meter), self.buffer),
                                 (self.buffer + (k * self.meter), self.size - self.buffer), 6)
                continue
            pygame.draw.line(self.game_display, self._BLACK, (self.buffer + (k * self.meter), self.buffer),
                             (self.buffer + (k * self.meter), self.size - self.buffer), 3)

    def draw_board_data(self):
        """
        populates the grid with the data held by the board object.
        Helper function for the draw_board func.
        :return:
        """
        for i in range(self.board.size):
            for j in range(self.board.size):
                if self.board.get_value(i, j) == 0:
                    continue
                text = self.font.render(str(self.board.get_value(i, j)), True, self._BLACK, self._WHITE)
                text_rect = text.get_rect()
                text_rect.center = ((self.buffer + (j * self.meter)) + self.meter / 2,
                                    (self.buffer + (i * self.meter)) + self.meter / 2)
                self.game_display.blit(text, text_rect)

    def draw_board(self):
        """
        Draws the full display board
        :return:
        """
        self.draw_board_grid()
        self.draw_board_data()

    def run(self):
        """
        main gameplay loop
        :return:
        """

        while not self.game_exit:
            self.game_display.fill(self._WHITE)
            self.clock.tick(self.FPS)
            self.draw_board()

            # Event loop
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.game_exit = True
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        self.game_exit = True
                    if event.key == pygame.K_SPACE:
                        self.board.solve()

            pygame.display.update()

        pygame.quit()


if __name__ == '__main__':
    my_game = Sudoku()
    my_game.run()
