import unittest

from boards.Board import Board


class BoardTest(unittest.TestCase):
    def test_load_board(self):
        with open("data/puzzles.txt", "r") as file:
            number_of_puzzles = int(file.readline())
            for i in range(number_of_puzzles):
                puzzle = file.readline()
                board = Board(sudoku_string=puzzle)
                self.assertEqual(puzzle.rstrip(), board.__repr__())
            file.close()

    def test_validate_valid(self):
        board = Board(sudoku_string="000060200"
                                    "001050000"
                                    "040000000"
                                    "600000500"
                                    "000100020"
                                    "003700000"
                                    "000401007"
                                    "800000300"
                                    "020000000")
        self.assertEqual(True, board.is_valid())

    def test_validate_invalid_col(self):
        board = Board(sudoku_string="800060200"
                                    "001050000"
                                    "040000000"
                                    "600000500"
                                    "000100020"
                                    "003700000"
                                    "000401007"
                                    "800000300"
                                    "020000000")
        self.assertEqual(False, board.is_valid())

    def test_validate_invalid_box(self):
        board = Board(sudoku_string="400060200"
                                    "001050000"
                                    "040000000"
                                    "600000500"
                                    "000100020"
                                    "003700000"
                                    "000401007"
                                    "800000300"
                                    "020000000")
        self.assertEqual(False, board.is_valid())

    def test_validate_invalid_row(self):
        board = Board(sudoku_string="200060200"
                                    "001050000"
                                    "040000000"
                                    "600000500"
                                    "000100020"
                                    "003700000"
                                    "000401007"
                                    "800000300"
                                    "020000000")
        self.assertEqual(False, board.is_valid())

    def test_reload_board(self):
        puzzle = "000060200001050000040000000600000500000100020003700000000401007800000300020000000"
        board = Board(sudoku_string=puzzle)
        self.assertEqual(puzzle, board.__repr__())
        for i in range(board.size):
            for j in range(board.size):
                board.set_value(i, j, -1)

        self.assertEqual("-1" * 81, board.__repr__())
        board.reload()
        self.assertEqual(puzzle, board.__repr__())

    def test_save_state_string(self):
        board = Board(sudoku_string="000060200"
                                    "001050000"
                                    "040000000"
                                    "600000500"
                                    "000100020"
                                    "003700000"
                                    "000401007"
                                    "800000300"
                                    "020000000")
        board.set_value(0, 0, 1)
        board.save_state_string()

        expected = "100060200001050000040000000600000500000100020003700000000401007800000300020000000"
        actual = board.state_string

        self.assertEqual(expected, actual)

    def test_is_solved_true(self):
        solved_puzzle = "517824936689513472234679158743256819951348267826791543392465781478132695165987324"

        b = Board(sudoku_string=solved_puzzle)
        is_solved = b.is_solved()
        self.assertEqual(is_solved, True)

    def test_is_solved_false(self):
        solved_puzzle = "517824936689513472234679158743256819951348267826791543392465781078132695165987324"

        b = Board(sudoku_string=solved_puzzle)
        is_solved = b.is_solved()
        self.assertEqual(is_solved, False)


if __name__ == '__main__':
    unittest.main()
