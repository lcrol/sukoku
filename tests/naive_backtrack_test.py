import random
import unittest

from boards.NaiveBackTrackSolver import NaiveBackTrackSolver


class NaiveBtTest(unittest.TestCase):
    def test_solve(self):
        with open("data/puzzles.txt", "r") as file:
            number_puzzles = int(file.readline())
            number_of_puzzles_tests = 1
            puzzles = file.readlines()
            for i in range(number_of_puzzles_tests):
                current_puzzle = puzzles[random.randint(0, number_puzzles)]
                board = NaiveBackTrackSolver(sudoku_string=current_puzzle)
                board.solve()
                self.assertEqual(board.is_solved(), True,
                                 f'Failing puzzle on line {current_puzzle}')
            file.close()


if __name__ == '__main__':
    unittest.main()
